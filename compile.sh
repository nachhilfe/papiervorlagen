#!/usr/bin/env bash

papersize_latex=("a4paper" "a4paper,landscape" "a5paper" "a5paper,landscape")
papersize_filename=("a4p" "a4l" "a5p" "a5l")
files=`find *.tex -not -name "tikz_settings.tex"`

for file in $files
do
  for (( i=0; i<${#papersize_latex[@]}; i++ ))
  do
    new_filename="${file%.tex}_"${papersize_filename[$i]}".tex"
    sed "s/a4paper/${papersize_latex[$i]}/" $file > $new_filename
    arara -v $new_filename
    rm $new_filename
  done
done
